package com.atlassian.annotation.indexer.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckResource {

    @RequestMapping("/healthcheck")
    public String check() {
        return "ok";
    }
}
