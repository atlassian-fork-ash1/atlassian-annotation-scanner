package com.atlassian.annotation.indexer.model.db;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DBTestRepository extends CrudRepository<DBTest, Long> {

    @Query("SELECT dbTestType.name, COUNT(dbTest.id) FROM DBTest dbTest INNER JOIN dbTest.testType dbTestType GROUP BY dbTestType.id")
    List getTestCountByType();

    @Query("SELECT categories.name, COUNT(dbTest.id) FROM DBTest dbTest INNER JOIN dbTest.categories categories GROUP BY categories.id ORDER BY 2 desc")
    List getTestCountByCategory();

    @Query("SELECT categories.name, COUNT(dbTest.id) FROM DBTest dbTest INNER JOIN dbTest.categories categories WHERE UPPER (categories.name) LIKE CONCAT('%',?1,'%') GROUP BY categories.id ORDER BY 2 desc")
    List getTestCountByCategory(String search);

    @Query("SELECT dbTest FROM DBTest dbTest INNER JOIN dbTest.testType dbTestType WHERE dbTestType.name = ?1 ORDER BY dbTest.testClass")
    List<DBTest> getTestsByType(String testType);

    @Query("SELECT dbTest FROM DBTest dbTest INNER JOIN dbTest.categories categories WHERE categories.name = ?1 ORDER BY dbTest.testClass")
    List<DBTest> getTestsByCategory(String category);

    @Query("SELECT dbTest FROM DBTest dbTest INNER JOIN dbTest.testType dbTestType WHERE dbTestType.name = ?1 AND UPPER (dbTest.name) LIKE CONCAT('%',?2,'%') ORDER BY dbTest.testClass")
    List<DBTest> getTestsByType(String testType, String search);

    @Query("SELECT dbTest FROM DBTest dbTest INNER JOIN dbTest.categories categories WHERE categories.name = ?1 AND UPPER (dbTest.name) LIKE CONCAT('%',?2,'%')  ORDER BY dbTest.testClass")
    List<DBTest> getTestsByCategory(String category, String search);


}



