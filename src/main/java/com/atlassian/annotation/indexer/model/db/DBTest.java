package com.atlassian.annotation.indexer.model.db;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "tests")
public class DBTest {

    @Id
    @Column(name = "test_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    long id;

    @Column(name = "test_name")
    String name;

    @Column(name = "test_jar")
    String jar;

    @Column(name = "test_package")
    String testPackage;

    @Column(name = "test_class")
    String testClass;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "test_category",
            joinColumns = @JoinColumn(name = "test_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    Set<DBTestCategory> categories;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "type_id")
    DBTestType testType;

    public String getTestPackage() {
        return testPackage;
    }

    public void setTestPackage(String testPackage) {
        this.testPackage = testPackage;
    }

    public String getTestClass() {
        return testClass;
    }

    public void setTestClass(String testClass) {
        this.testClass = testClass;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJar() {
        return jar;
    }

    public void setJar(String jar) {
        this.jar = jar;
    }

    public Set<DBTestCategory> getCategories() {
        return categories;
    }

    public void setCategories(Set<DBTestCategory> categories) { this.categories = categories; }

    public DBTestType getTestType() { return testType; }

    public void setTestType(DBTestType testType) { this.testType = testType; }

}
