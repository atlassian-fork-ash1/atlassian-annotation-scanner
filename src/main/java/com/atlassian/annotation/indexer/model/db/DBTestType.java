package com.atlassian.annotation.indexer.model.db;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "type")
public class DBTestType {

    @Id
    @Column(name = "type_id")
    @GeneratedValue(strategy= GenerationType.AUTO)
    long id;

    @Column(name = "type_name")
    String name;

    @OneToMany(mappedBy = "testType")
    List<DBTest> tests;

    public long getId() { return id; }

    public void setId(long id) { this.id = id; }

    public List<DBTest> getTests() { return tests; }

    public void setTests(List<DBTest> tests) { this.tests = tests; }

    public String getName() { return name; }

    public void setName(String name) { this.name = name; }

}
