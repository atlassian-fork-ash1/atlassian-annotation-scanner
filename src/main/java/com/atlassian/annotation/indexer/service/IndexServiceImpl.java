package com.atlassian.annotation.indexer.service;

import com.atlassian.annotation.indexer.model.atr.Gavs;
import com.atlassian.annotation.indexer.model.atr.TestGav;

import com.atlassian.annotation.indexer.model.tests.Test;
import com.atlassian.annotation.indexer.model.tests.Tests;
import org.jboss.jandex.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Service
public class IndexServiceImpl implements IndexService {

    private static final Logger log = LoggerFactory.getLogger(IndexServiceImpl.class);

    @Autowired
    DatabaseService databaseService;

    @Autowired
    MavenService mavenService;

    @Transactional
    @Async
    public void indexTests(Gavs gavs, String testType) {
        Map<String, Index> indexes = new HashMap<String, Index>();
        int count = 1;
        for (TestGav testGav : gavs.getTest_gavs()) {
            log.info(String.format("Test type is %s - Indexing artifacts %s/%s %s", testType, count++, gavs.getTest_gavs().length, testGav.getArtifactId()));
            try {
                indexes.put(testGav.getArtifactId(), indexJar(mavenService.getMavenArtifact(testGav)));
            } catch (FileNotFoundException e) {
                log.warn(String.format("%s not found in maven", testGav.getArtifactId()));
            }
        }
        log.info("Indexing complete");

        Tests tests = this.getTestByCategoriesByJar(indexes);
        for (Test test:tests.getTests()) {
            test.setTestType(testType);
        }

        databaseService.addTests(tests, testType);
    }

    @Transactional
    @Async
    public void indexTests(InputStream jarInputStream, String testType, String artifactId) {
        Map<String, Index> indexes = new HashMap<String, Index>();
        log.info(String.format("Test type is %s - Indexing artifact %s", testType, artifactId));
        indexes.put(artifactId, indexJar(jarInputStream));
        log.info("Indexing complete");

        Tests tests = this.getTestByCategoriesByJar(indexes);
        for (Test test:tests.getTests()) {
            test.setTestType(testType);
        }

        databaseService.addTests(tests, testType);
    }

    @Override
    public Index indexJar(InputStream jarInputStream) {
        Indexer indexer = new Indexer();
        ZipInputStream zin = new ZipInputStream(jarInputStream);
        ZipEntry ze = null;
        try {
            while ((ze = zin.getNextEntry()) != null) {
                String entryName = ze.getName();
                if (entryName.endsWith("class")) {
                    ByteArrayOutputStream bout = new ByteArrayOutputStream();
                    for (int c = zin.read(); c != -1; c = zin.read()) {
                        bout.write(c);
                    }
                    zin.closeEntry();
                    bout.close();
                    indexer.index(new ByteArrayInputStream(bout.toByteArray()));
                }
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return indexer.complete();
    }

    @Override
    public Tests getTestByCategoriesByJar(Map<String, Index> indexes) {

        DotName jiraWebTestAnnotation = DotName.createSimple("com.atlassian.jira.functest.framework.suite.WebTest");
        DotName categoryAnnotation = DotName.createSimple("org.junit.experimental.categories.Category");
        DotName testAnnotation = DotName.createSimple("org.junit.Test");
        Tests tests = new Tests();
        List<Test> testList = new ArrayList<Test>();

        for (String eachJar : indexes.keySet()) {

            List<AnnotationInstance> annotations = new ArrayList<>();
            annotations.addAll(indexes.get(eachJar).getAnnotations(categoryAnnotation));
            annotations.addAll(indexes.get(eachJar).getAnnotations(jiraWebTestAnnotation));

            for (AnnotationInstance annotation : annotations) {

                Set<String> categories = new HashSet<String>();
                for (AnnotationValue value : annotation.values()) {

                    try {
                        for (Type type : value.asClassArray()) {
                            categories.add(type.toString());
                        }
                    } catch (IllegalArgumentException e) {
                        for (String type : value.asEnumArray()) {
                            categories.add(type.toString());
                        }
                    }
                }

                switch (annotation.target().kind()) {
                    case CLASS:
                        for (MethodInfo method : annotation.target().asClass().methods()) {
                            if (method.asMethod().hasAnnotation(testAnnotation)) {
                                Test test = new Test(eachJar, sanitizeTestName(method.toString()), annotation.target().asClass().name().prefix().toString(),  annotation.target().asClass().name().local());
                                test.setCategories(categories);
                                testList.add(test);
                            }
                        }
                        break;
                    case METHOD:
                        Test test = new Test(eachJar, sanitizeTestName(annotation.target().toString()), annotation.target().asMethod().declaringClass().name().prefix().toString(), annotation.target().asMethod().declaringClass().name().local());
                        testList.add(test);
                        break;
                }
            }
        }
        tests.setTests(testList);
        return tests;
    }

    private String sanitizeTestName(String testName) {

        Pattern p = Pattern.compile("(\\S+)\\(\\)");   // the pattern to search for
        Matcher m = p.matcher(testName);

        if (m.find()) {
            return m.group(0);
        } else {
            return testName;
        }
    }
}
