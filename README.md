# atlassian-annotation-scanner

Download and index test JARs artifacts from Nexus maven repository
Provides UI to visualise and search test categories

https://jdog.jira-dev.com/secure/RapidBoard.jspa?rapidView=1309&projectKey=JTA
HipChat: JIRA Test Audit

## Getting started

### Local running postgres

Requires local running postgres.
Local database onfiguration in src/main/resources/application.properties

### To index artifacts via repo download

Bamboo indexing plan https://jira-cloud-bamboo.internal.atlassian.com/browse/QA-JTP

Use post-example.json file format (the same created by acceptance-test-runner)
curl -vX POST http://localhost:8080/index/indexGavs/func --header "Content-Type: application/json" --data @http-post-example-jira-webdriver-tests.json
curl -vX POST http://localhost:8080/index/indexGavs/atslats --header "Content-Type: application/json" --data @http-post-example-atslats.json

### To index file via POST upload

curl -X POST -H "Content-Type: multipart/form-data" -F "file=@../jira/jira-functional-tests/jira-func-tests/target/jira-func-tests-1001.0.0-SNAPSHOT.jar" "http://localhost:8080/index/artifact/func-tests"

### UI
Angular JS exposed at
localhost:8080/

### To deploy on micros

Bamboo plan https://engservices-bamboo.internal.atlassian.com/browse/QA-AAS

Requires running micros-cli https://extranet.atlassian.com/display/MICROS/Quick+installation+steps+for+micros-cli
See micros-deploy.sh
