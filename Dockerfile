FROM java:8

RUN apt-get update
RUN apt-get install -y nginx supervisor

COPY etc/supervisor.conf /etc/supervisor/conf.d/
COPY etc/nginx.conf /etc/nginx/
COPY target/atlassian-annotation-scanner-0.0.1-SNAPSHOT.jar /

CMD ["/usr/bin/supervisord"]
